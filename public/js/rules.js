jQuery.validator.addMethod("lettersAndSpacesOnly", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Digite apenas letras"); 

$('document').ready(function(){
    $("#frmCadUsuario").validate({
        rules:{
            name:{
                required: true,
                lettersAndSpacesOnly: true 
            },
            rg:{
                required: true,
            },
            cpf:{
                required: true,
            },
            phone:{
                required: true
            },
            cep:{
                required: true                   
            },
            number:{
                required: true,
                number: true
            }               
        }
    })
    
    $("#cpf").mask("000.000.000-00") 
    $("#cep").mask("00000-000")  
})