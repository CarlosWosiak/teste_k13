       $(document).ready(function() {

            function clean_cep_form() {
                $("#street").val("")
                $("#neighborhood").val("")
                $("#city").val("")
                $("#state").val("")
                $("#ibge").val("")
            }
            
            $("#cep").blur(function() {

                //New "cep" variable with numbers only
                var cep = $(this).val().replace(/\D/g, '')

                //Verify if the CEP field has any value
                if (cep != "") {

                    var cep_validate = /^[0-9]{8}$/

                    if(cep_validate.test(cep)) {

                        //Fill the fields with "..." while consulting the webservice
                        $("#street").val("...")
                        $("#neighborhood").val("...")
                        $("#city").val("...")
                        $("#state").val("...")

                        //Search in webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(data) {

                            if (!("erro" in data)) {
                                $("#street").val(data.logradouro)
                                $("#neighborhood").val(data.bairro)
                                $("#city").val(data.localidade)
                                $("#state").val(data.uf)
                            }
                            else {
                                clean_cep_form()
                            }
                        })
                    } 
                    else {
                        clean_cep_form()
                    }
                } 
                else {
                    clean_cep_form()
                }
            })
        })

