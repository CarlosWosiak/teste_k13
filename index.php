<?php

require("config/config.php");

function __autoload($class)
{
    $dirs  = Array("core/controller", "core/model",
        "core/view", "model" , "view", "controller");
    
    foreach($dirs as $dir)
    {
        $path = $dir . "/" . $class . ".class.php";
        if(file_exists($path))
        {
            require_once($path);
        }
    }
}

echo App::run();


