<?php
class Client 
{
    public function show()
    {
        $html = Html::load("view/html/client.html");
        $grid = "";
        
        $clientModel = new ClientModel();
        $dataArray = $clientModel->listClients();
        if($dataArray[0] == true)
        {
            if($dataArray[1] != null){
                foreach($dataArray[1] as $objClientModel)
                {
                    $grid .= "<tr>"; 
                    $grid .= "<td>" . $objClientModel->getClient_Name() . "</td>";
                    $grid .= "<td>" . $objClientModel->getClient_Sex() . "</td>";
                    $grid .= "<td>" . $objClientModel->getClient_Rg() . "</td>";
                    $grid .= "<td><a href='#URL#/Client/delete?id=". $objClientModel->getClient_id()."&address=". $objClientModel->getClient_address()."' class='btn btn-danger'> Deletar</a>
                    <a href='#URL#/Client/edit?c=". $objClientModel->getClient_id()."&ad=". $objClientModel->getClient_address()."' class='btn btn-primary'> Editar</a>
                    </td>"; 
                    $grid .= "</tr>";
                }
            }else{
                $grid .= "<tr>"; 
                    $grid .= "<td> </td>";
                    $grid .= "<td> </td>";
                    $grid .= "<td> </td>";
                    $grid .= "<td> </td>"; 
                    $grid .= "</tr>";
            } 
        }else
        {
            return $dataArray[1];
        }
        
        
        $html = str_replace("#GRID#", $grid, $html);
        
        return $html;
    }
    
    public function newClient()
    {
        $html = Html::load("view/html/client-form.html");
        $html = str_replace("#TITULO#", "Novo Cliente", $html);
        
        return $html;
    }

    public function edit()
    {
        $clientModel = new ClientModel();
        $phoneModel = new Phone_NumberModel();
        $data = $clientModel->search(filter_input(INPUT_GET, "c"));
        $phone = $phoneModel->search(filter_input(INPUT_GET, "c"));

        $html = Html::load("view/html/client-edit-form.html");
        $html = str_replace("#ID#",$data['client_id'] , $html);
        $html = str_replace("#IDADDRESS#", filter_input(INPUT_GET, "ad") , $html);
        $html = str_replace("#NAME#",$data['client_name'] , $html);
        $html = str_replace("#TITULO#","Editar cliente", $html);

        if($data['client_sex'] == 'M'){
            $html = str_replace("#CHECKEDMALE#","checked='checked'", $html);
            $html = str_replace("#CHECKEDFEMALE#","Editar cliente=''", $html);
        }else{
            $html = str_replace("#CHECKEDFEMALE#","checked='checked'", $html);
            $html = str_replace("#CHECKEDMALE#","Editar cliente=''", $html);
        }
        
        $html = str_replace("#RG#",$data['client_rg'] , $html);
        $html = str_replace("#CPF#",$data['client_cpf'] , $html);
       
        $phoneReplace = '
        <td>
            <input maxlength="20" type="text" name="phone[]" id="phone" 
            class="form-control name_list"value="'
            .$phone[0].
            '"/>
        </td>
        <td>
        <button type="button" name="add" id="add" class="btn btn-success">+
        </button>
        </td>';

        for($i = 1; $i < count($phone); $i++){
            $phoneReplace .= '
            <tr id="row'.$i.'">
                <td>
                <input  maxlength="20" type="text" value="'.$phone[$i].'" name="phone[]" id="phone" class="form-control name_list" />
                </td>
                <td><button type="button" name="remove" id="'.$i.'" class="btn btn-danger btn_remove">X</button>
                </td>
            </tr>';
        }
        

        $html = str_replace("#PHONE#",$phoneReplace,$html);


        
        $html = str_replace("#CEP#",$data['address_cep'] , $html);
        $html = str_replace("#STREET#",$data['address_street'] , $html);
        $html = str_replace("#NUMBER#",$data['address_number'] , $html);
        $html = str_replace("#COMPLEMENT#",$data['address_complement'] , $html);
        $html = str_replace("#NEIGHBORHOOD#",$data['address_neighborhood'] , $html);
        $html = str_replace("#CITY#",$data['address_city'] , $html);
        $html = str_replace('value="'.$data['address_state'].'"','value="'.$data['address_state'].'" selected', $html);
        
        return $html;
    }
    
    public function save()
    {
            //Create the address to bind in the client
            $clientAddress = new AddressModel();
            $clientAddress->setAddress_cep(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST,"cep")));
            $clientAddress->setAddress_street(filter_input(INPUT_POST,"street"));
            $clientAddress->setAddress_number(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST,"number")));
            $clientAddress->setAddress_complement(filter_input(INPUT_POST,"complement"));
            $clientAddress->setAddress_neighborhood(filter_input(INPUT_POST,"neighborhood"));
            $clientAddress->setAddress_city(filter_input(INPUT_POST,"city"));
            $clientAddress->setAddress_state(filter_input(INPUT_POST,"state"));
            $dataAddress = $clientAddress->save();

            //Create the Client
            $clientModel = new ClientModel();          
            $clientModel->setClient_name(preg_replace('/[^a-zA-Z\s]/', '',filter_input(INPUT_POST, "name")));
            $clientModel->setClient_sex(filter_input(INPUT_POST, "sex"));
            $clientModel->setClient_rg(filter_input(INPUT_POST, "rg"));
            $clientModel->setClient_cpf(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST, "cpf")));
            $clientModel->setClient_address($dataAddress[1]);
            $dataClient = $clientModel->save();

            //Create the Phone binding the client to it
            foreach ($_POST['phone'] as  $value) {
                $value = preg_replace('/[^0-9]/', '',$value);
                $clientPhone = new Phone_NumberModel();
                $clientPhone->setPhone_number($value);
                $clientPhone->setPhone_number_client($dataClient[1]);
                $dataPhone = $clientPhone->save();
            }
            


            if( ($dataClient[0] == true) && ($dataPhone[0] == true) && ($dataAddress[0] == true) )
                return "Dados gravados com sucesso!";
            else
                return "Erro ao salvar" . $dataAddress[1];

    }

    public function update(){
        $clientAddress = new AddressModel();
        $clientAddress->setAddress_cep(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST,"cep")));
        $clientAddress->setAddress_street(filter_input(INPUT_POST,"street"));
        $clientAddress->setAddress_number(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST,"number")));
        $clientAddress->setAddress_complement(filter_input(INPUT_POST,"complement"));
        $clientAddress->setAddress_neighborhood(filter_input(INPUT_POST,"neighborhood"));
        $clientAddress->setAddress_city(filter_input(INPUT_POST,"city"));
        $clientAddress->setAddress_state(filter_input(INPUT_POST,"state"));
        $dataAddress = $clientAddress->update(filter_input(INPUT_POST,"idAddress"));

        //Create the Client
        $clientModel = new ClientModel();          
        $clientModel->setClient_name(preg_replace('/[^a-zA-Z\s]/', '',filter_input(INPUT_POST, "name")));
        $clientModel->setClient_sex(filter_input(INPUT_POST, "sex"));
        $clientModel->setClient_rg(filter_input(INPUT_POST, "rg"));
        $clientModel->setClient_cpf(preg_replace('/[^0-9]/', '',filter_input(INPUT_POST, "cpf")));
        $dataClient = $clientModel->update(filter_input(INPUT_POST,"id"));

        //Create the Phone binding the client to it
        $clientPhone = new Phone_NumberModel();        
        $clientPhone->deleteAll(filter_input(INPUT_POST,"id"));

        foreach ($_POST['phone'] as  $value) {
            $value = preg_replace('/[^0-9]/', '',$value);
            $clientPhoneEdit = new Phone_NumberModel();
            $clientPhoneEdit->setPhone_number($value);
            $clientPhoneEdit->setPhone_number_client(filter_input(INPUT_POST,"id"));            
            $dataPhone = $clientPhoneEdit->save();
        }

        

        

        if( ($dataClient[0] == true) && ($dataPhone[0] == true) && ($dataAddress[0] == true) )
            return "Dados gravados com sucesso!";
        else
            return "Erro ao salvar" . $dataClient[1] . $dataPhone[1] . $dataAddress[1];
        
    }

    public function delete()
    {
            //Delete the Client (cascading deleting the phone)
            $clientModel = new ClientModel();
            $clientModel->setClient_id(filter_input(INPUT_GET, "id"));
            $dataClient = $clientModel->delete();
                        
            //Delete the Address
            $addressModel = new AddressModel();
            $addressModel->setAddress_id(filter_input(INPUT_GET, "address"));
            $dataAddress = $addressModel->delete();

            if(($dataClient[0] == true) && ($dataAddress[0]))
                return "Cliente deletado com sucesso!";
            else
                return "Erro ao salvar";
    }
}
