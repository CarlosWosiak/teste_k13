<?php

class App 
{
    public static $module;
    public static $action;
    public static $key;
    
    public static function run()
    {
        $url = filter_input(INPUT_GET, "url");
        $arrayUrl = explode("/",$url);
        
        
        self::$module = isset($arrayUrl[0]) && $arrayUrl[0] != "" ? $arrayUrl[0] : "Client";
        self::$action = isset($arrayUrl[1]) && $arrayUrl[1] != "" ? $arrayUrl[1] : "show"; 
        

        return Template::run();
    }
}
