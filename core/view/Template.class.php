<?php

class Template 
{
    private static $html;
    
    public static function run()
    {
        $module_class = App::$module;
        if(class_exists($module_class))
        {
            $object = new $module_class;
            if(method_exists($object, App::$action))
            {
                $metodo = App::$action;
                self::$html = $object->$metodo();
            }
            else
            {
                self::$html = "A ação [" . App::$action .
                        "] não existe no módulo [" . $module_class . "]";
            }
        }
        else
        {
            self::$html = "O módulo [" . $module_class . "] não existe";
        }
        
        $template = Html::load("view/html/index.html");
                
        self::$html = str_replace("#CONTENT#", self::$html, $template);
        self::$html = str_replace("#URL#", URL, self::$html);
        return self::$html;
    }
}
