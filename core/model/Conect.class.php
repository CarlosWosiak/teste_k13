<?php


class Conect 
{
    
    private static $conect;
    
    private function __construct() {}
    
    public static function getConect() : PDO
    {
        if(empty(self::$conect))
        {
            self::$conect = new PDO("mysql:host=localhost;dbname=teste_k13","root","");
            self::$conect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        return self::$conect;
    }
}
