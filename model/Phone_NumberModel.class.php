<?php

class Phone_NumberModel
{
    private $Phone_number_id;
    private $Phone_number;
    private $Phone_number_client;

    
    public function getPhone_number_id()
    {
        return $this->Phone_number_id;
    }

    public function setPhone_number_id($Phone_number_id)
    {
        $this->Phone_number_id = $Phone_number_id;
    }
    
    public function getPhone_number()
    {
        return $this->Phone_number;
    }

    public function setPhone_number($Phone_number)
    {
        $this->Phone_number = $Phone_number;
    }
    


    public function getPhone_number_client()
    {
        return $this->Phone_number_client;
    }


    public function setPhone_number_client($Phone_number_client)
    {
        $this->Phone_number_client = $Phone_number_client;
    }
    
    
    
    public function save()
    {
        try
        {
            $sql = 
            "INSERT INTO phone_number(phone_number,phone_number_client)
            VALUES (:phone_number,:client)";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":phone_number", $this->getPhone_number());            
            $query->bindValue(":client", $this->getPhone_number_client());
            $query->execute();
            return Array(true);
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function delete() 
    { 
        try{
            $sql = "DELETE FROM phone_number WHERE phone_number_id = :id";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":id", $this->getPhone_number_id());        
            $query->execute();
            return Array(true);           
            
        }catch(PDOException $e){
            return Array(false, $e->getMessage());
        }        
    } 

    public function deleteAll($id) 
    { 
        try{
            $sql = "DELETE FROM phone_number WHERE phone_number_client = $id";
            $query = Conect::getConect()->prepare($sql);
            $query->execute();
            return Array(true);           
            
        }catch(PDOException $e){
            return Array(false, $e->getMessage());
        }        
    } 
    
    public function search($id)
    {
        try
        {
            
            $sql = "SELECT * FROM phone_number p
            JOIN client c ON p.phone_number_client = c.client_id
            WHERE p.phone_number_client = :id";
            
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":id", $id);
            $query->execute();
            $allPhoneNumbers = [];
            $clientRow = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach($clientRow as $data){
                array_push($allPhoneNumbers,$data['phone_number']);
            };
            return $allPhoneNumbers;
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }

    public function update($id)
    {
        try
        {
            $sql = "UPDATE phone_number
            SET phone_number = :phone
            WHERE phone_number_client = $id";

            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":phone", $this->getPhone_number());
            $query->execute();
            return Array(true);
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
}
