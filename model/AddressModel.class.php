<?php

class AddressModel
{
    private $address_id;
    private $address_cep;
    private $address_street;
    private $address_number;
    private $address_complement;
    private $address_neighborhood;
    private $address_city;
    private $address_state;
    
    public function getAddress_id()
    {
        return $this->address_id;
    }

    public function setAddress_id($address_id)
    {
        $this->address_id = $address_id;

    }

    public function getAddress_cep()
    {
        return $this->address_cep;
    }

    public function setAddress_cep($address_cep)
    {
        $this->address_cep = $address_cep;

    }

    public function getAddress_street()
    {
        return $this->address_street;
    }

    public function setAddress_street($address_street)
    {
        $this->address_street = $address_street;

    }

    public function getAddress_number()
    {
        return $this->address_number;
    }

    public function setAddress_number($address_number)
    {
        $this->address_number = $address_number;

    }

    public function getAddress_complement()
    {
        return $this->address_complement;
    }

    public function setAddress_complement($address_complement)
    {
        $this->address_complement = $address_complement;

    }

    public function getAddress_neighborhood()
    {
        return $this->address_neighborhood;
    }

    public function setAddress_neighborhood($address_neighborhood)
    {
        $this->address_neighborhood = $address_neighborhood;
    }

    
    public function getAddress_city()
    {
        return $this->address_city;
    }

    public function setAddress_city($address_city)
    {
        $this->address_city = $address_city;

    }

    public function getAddress_state()
    {
        return $this->address_state;
    }

    public function setAddress_state($address_state)
    {
        $this->address_state = $address_state;

    }
    
    public function save()
    {
        try
        {
            $sql = 
            "INSERT INTO address(address_cep,address_street,address_number,address_complement,address_neighborhood, address_city, address_state)
            VALUES (:cep, :street, :number, :complement, :neighborhood, :city, :state)";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":cep", $this->getAddress_cep());
            $query->bindValue(":street", $this->getAddress_street());
            $query->bindValue(":number", $this->getAddress_number());
            $query->bindValue(":complement", $this->getAddress_complement());
            $query->bindValue(":neighborhood", $this->getAddress_neighborhood());
            $query->bindValue(":city", $this->getAddress_city());
            $query->bindValue(":state", $this->getAddress_state());
            $query->execute();
            return Array(true, Conect::getConect()->lastInsertId());
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function update($id)
    {
        try
        {
            $sql = "UPDATE address
            SET 
            address_cep = :cep,
            address_street = :street,
            address_number = :number,
            address_complement = :complement,
            address_neighborhood = :neighborhood,
            address_city = :city,
            address_state = :state
            WHERE address_id = $id";

            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":cep", $this->getAddress_cep());
            $query->bindValue(":street", $this->getAddress_street());
            $query->bindValue(":number", $this->getAddress_number());
            $query->bindValue(":complement", $this->getAddress_complement());
            $query->bindValue(":neighborhood", $this->getAddress_neighborhood());
            $query->bindValue(":city", $this->getAddress_city());
            $query->bindValue(":state", $this->getAddress_state());
            $query->execute();
            return Array(true, Conect::getConect()->lastInsertId());
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function delete() 
    { 
        try{
            $sql = "DELETE FROM address WHERE address_id = :id";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":id", $this->getAddress_id());
            var_dump($query);
        
            $query->execute();
            return Array(true);           
            
        }catch(PDOException $e){
            return Array(false, $e->getMessage());
        }        
    }   

}
