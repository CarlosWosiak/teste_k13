<?php

class ClientModel
{
    private $Client_id;
    private $Client_name;
    private $Client_sex;
    private $Client_rg;
    private $Client_cpf;
    private $Client_address;
    
    public function getClient_id()
    {
        return $this->Client_id;
    }
    
    public function setClient_id($Client_id)
    {
        $this->Client_id = $Client_id;
    
        return $this;
    }
    
    public function getClient_name()
    {
        return $this->Client_name;
    }
    
    public function setClient_name($Client_name)
    {
        $this->Client_name = $Client_name;
    
        return $this;
    }
    
    public function getClient_sex()
    {
        return $this->Client_sex;
    }
    
    public function setClient_sex($Client_sex)
    {
        $this->Client_sex = $Client_sex;
    
        return $this;
    }
    
    public function getClient_rg()
    {
        return $this->Client_rg;
    }
    
    public function setClient_rg($Client_rg)
    {
        $this->Client_rg = $Client_rg;
    
        return $this;
    }
    
    public function getClient_cpf()
    {
        return $this->Client_cpf;
    }
    
    public function setClient_cpf($Client_cpf)
    {
        $this->Client_cpf = $Client_cpf;
    
        return $this;
    }
    
    public function getClient_address()
    {
        return $this->Client_address;
    }
    
    public function setClient_address($Client_address)
    {
        $this->Client_address = $Client_address;
    
        return $this;
    }    
    
    
    public function listClients()
    {
        try
        {
            $sql = "SELECT * FROM client";
            $query = Conect::getConect()->prepare($sql);
            $query->execute();
            $arrayData = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach($arrayData as $data)
            {
                $clientModel = new clientModel();
                $clientModel->setClient_id($data["client_id"]);
                $clientModel->setClient_name($data["client_name"]);
                $clientModel->setClient_sex($data["client_sex"]);
                $clientModel->setClient_rg($data["client_rg"]);
                $clientModel->setClient_cpf($data["client_cpf"]);
                $clientModel->setClient_address($data["client_address"]);              
                
                $arrayObjects[] = $clientModel;
            }
            if(isset($arrayObjects)){
                return Array(true, $arrayObjects);
            }else{
                return Array(true, []);
            }
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }


    public function search($id)
    {
        try
        {
            
            $sql = "SELECT * FROM client c
            JOIN address a on a.address_id = c.client_address
            WHERE client_id=:id";
            
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":id", $id);
            $query->execute();
            $clientRow = $query->fetchAll(PDO::FETCH_ASSOC);
            return $clientRow[0];
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function save()
    {
        try
        {
            $sql = 
            "INSERT INTO client(client_name, client_sex, client_rg, client_cpf, client_address)
            VALUES (:name, :sex, :rg, :cpf, :address)";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":name", $this->getClient_name());
            $query->bindValue(":sex", $this->getClient_sex());
            $query->bindValue(":rg", $this->getClient_rg());
            $query->bindValue(":cpf", $this->getClient_cpf());
            $query->bindValue(":address", $this->getClient_address());
            $query->execute();
            return Array(true, Conect::getConect()->lastInsertId());
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function update($id)
    {
        try
        {
            $sql = "UPDATE client
            SET 
            client_name = :name,
            client_sex = :sex,
            client_rg = :rg,
            client_cpf = :cpf
            WHERE client_id = $id ;";
            
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":name", $this->getClient_name());
            $query->bindValue(":sex", $this->getClient_sex());
            $query->bindValue(":rg", $this->getClient_rg());
            $query->bindValue(":cpf", $this->getClient_cpf());
            $query->execute();
            return Array(true);
        } 
        catch (PDOException $e) 
        {
            return Array(false, $e->getMessage());
        }
    }
    
    public function delete() 
    { 
        try{
            $sql = "DELETE FROM client WHERE client_id = :id";
            $query = Conect::getConect()->prepare($sql);
            $query->bindValue(":id", $this->getClient_id());
            var_dump($query);
        
            $query->execute();
            return Array(true);           
            
        }catch(PDOException $e){
            return Array(false, $e->getMessage());
        }        
    }   
}
